/* fuzz version 0.5 11/16/1999
   Copyright Ben Woodard <ben@valinux.com> & VA Linux Systems.
   All rights reserved.
   Licence: GPL

   The purpose of this program is to generate random garbage and 
   pass that random garbage into a program to see if it can be 
   made to crash or hang. It will pass this garbage into the 
   program in one of two ways. The first is on the command line 
   and the second is on through standard in. It runs the victim
   program a specified number of times dumping any output out to 
   /dev/null. If it manages to get the program to crash, it stops.
   The reason for this is debatable but reasonable. I figured that
   once we have discovered one exploit it is best to stop and fix
   it rather than discovering literally hundreds of ways of 
   triggering the same bug.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#include <pwd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>


#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#else
// Little replacement for readline if it is not available
char *chomp(char *str);
char *readline(const char *prompt){
  char *retval;
  
  printf("%s",prompt);
  retval=malloc(10240);
  retval[10239]=0;
  fgets(retval,10239,stdin);
  return chomp(retval);
}
#endif

#define BADOPS_EXIT 1
#define CANT_CHROOT_EXIT 2
#define CANT_EXEC_EXIT 3
#define NORAND_EXIT 4
#define ARGFILE_EXIT 5
#define NOMEM_EXIT 6
#define CANT_FORK_EXIT 7
#define CHILDFD_EXIT 8
#define NORCFILE_EXIT 9
#define NOREPORT_EXIT 10

#define RESULT_PASSED 1
#define RESULT_FAILED 0

/* borrowed this from vixie-cron 
 -Fx	 = set full-name of sender
 -odi	 = Option Deliverymode Interactive
 -oem	 = Option Errors Mailedtosender
 -or0s = Option Readtimeout -- don't time out (which doesn't seem
         to work for fuzz)
*/
#define SENDMAILARGS "-odi -oem -Ffuzz"
#define DESTADDR "fuzzmonster@zgp.org"

/* Probably the best way to do this is to make the RAND_DEVICE
   /dev/random however this requires user input and so it it 
   really won't work when you need a large amount of data.  It 
   will literally take a couple of seconds for each couple of 
   bytes.  
*/
#define RAND_DEVICE "/dev/urandom"
// #define RAND_DEVICE "/dev/random"

#define DEFRUNS 10000
#define DEFLEN 100000
#define DEFTIMEOUT 120

#define MAXPATH 10240
#define MAXARGS 256
#define MAXARGLEN 256

/* global variables
   These are either needed by the signal handlers or send_report 
   which is called by the signal handlers */
unsigned long runs=DEFRUNS; 
char progname[MAXPATH];      
char outfilename[MAXPATH];   
pid_t newpid;                
char rundone=0;              
unsigned long max_args=0; 
char *argfilename;        
int g_argc;
char **g_argv;
int report;
char sendmail[MAXPATH];
char distribution[MAXPATH];
char dontask=0;

void print_arglist(FILE*,char **,char,char);
int setup_user(char *distribution, char *sendmail);
void handle_sigalrm(int dummy);
void handle_sigchld(int dummy);
void do_child(int *progpipe, char **argv, char *execute_filename, 
	      unsigned long max_arglen,char printable_only,int nullfd,
	      int randfd);
void send_report(char result, char report, char *sendmail, char *distribution);

char *chomp(char * str){
  int i=strlen(str);
  if(str[i-1]=='\n')
    str[i-1]=0;
  return str;
}

void usage(){
  fprintf(stderr,"usage: fuzz [-p] [-r runcount] [-p] [-n linemod] [-l length] [-m maxlinelen]\n            [-t timeout] [-c] [-u user] [-x maxargs] [-y maxarglen] [-e number]\n            command [arg...]\n");
  exit(BADOPS_EXIT); 
}

int main(int argc, char **argv){
  char binfound=0;
  unsigned long max_arglen=MAXARGLEN;
  char *execute_filename=NULL;
  char printable_only=0;
  int curarg;
  unsigned long len=DEFLEN;
  unsigned long timeout=DEFTIMEOUT;
  unsigned long maxline=0;
  unsigned long linemod=0;
  char print_bytes=0;
  char omit_data=0;
  char chr=0;
  int prio;
  static const struct option longopts[]={
    {"args",      no_argument,      NULL,'a'},
    {"bytes",     no_argument,      NULL,'b'},
    {"chroot",    no_argument,      NULL,'c'},
    {"dontask",   no_argument,      NULL,'d'},
    {"execute",   required_argument,NULL,'e'},
    {"priority",  required_argument,NULL,'i'},
    {"length",    required_argument,NULL,'l'},
    {"maxline",   required_argument,NULL,'m'},
    {"newlines",  required_argument,NULL,'n'},
    {"omitdata",  no_argument,      NULL,'o'},
    {"printable", no_argument,      NULL,'p'},
    {"runcount",  required_argument,NULL,'r'},
    {"timeout",   required_argument,NULL,'t'},
    {"user",      required_argument,NULL,'u'},
    {"version",   no_argument,      NULL,'V'},
    {"maxargs",   required_argument,NULL,'x'},
    {"maxarglen", required_argument,NULL,'y'},
    NULL
  };
  char *path;
  char bin_found=0;
  FILE *outfile;
  FILE *infile;
  struct sigaction act; 
  struct passwd *userinfo=NULL;
  int nullfd;
  int randfd;

  g_argc=argc;
  g_argv=argv;
  while((curarg=getopt_long(argc,argv,"+a::bcde:i:l:m:n:opr:t:u:Vx:y:",
			    longopts,NULL))!=EOF){
    switch(curarg){
    case 'r':
      if(sscanf(optarg,"%ul",&runs)!=1){
	fprintf(stderr,"Bad number of runs.\n");
	usage();
      }
      break;
    case 'l':
      if(sscanf(optarg,"%ul",&len)!=1){
	fprintf(stderr,"Bad length of data stream.\n");
	usage();
      }
      break;
    case 'p':
      printable_only=1;
      break;
    case 'n':
      if(sscanf(optarg,"%ul",&linemod)!=1){
	fprintf(stderr,"Bad line length modifier.\n");
	usage();
      }
      break;
    case 'm':
      if(sscanf(optarg,"%ul",&maxline)!=1){
	fprintf(stderr,"Bad max line length.\n");
	usage();
      }      
      break;
    case 't':
      if(sscanf(optarg,"%ul",&timeout)!=1){
	fprintf(stderr,"Bad max line length.\n");
	usage();
      }      
      break;
    case 'b':
      print_bytes=1;
      break;
    case 'c':
      chr=1;
      break;
    case 'u':
      if((userinfo=getpwnam(optarg))==NULL){
	fprintf(stderr,"Can't look up user id.\n");
	perror(argv[0]);
	exit(BADOPS_EXIT);
      }
      break;
    case 'o':
      omit_data=0;
      break;
    case 'a':
      max_args=MAXARGS;
      break;
    case 'x':
      if(sscanf(optarg,"%ul",&max_args)!=1){
	fprintf(stderr,"Bad max number of args.\n");
	usage();
      }
      break;
    case 'y':
      if(sscanf(optarg,"%ul",&max_arglen)!=1){
	fprintf(stderr,"Bad max argument length.\n");
	usage();
      }
      break;
    case 'e':
      execute_filename=optarg;
      runs=1;
      break;
    case 'i':
      if(sscanf(optarg,"%i",&prio)!=1){
	fprintf(stderr,"Bad priority argument.\n");
	usage();
      }
      if (setpriority(PRIO_PROCESS,0,prio)) {
	fprintf(stderr,"Error setting nice priority.\n");
	perror(argv[0]);
	}
      break;
    case 'V':
      printf("full %s\n",VERSION);
      exit(0);
    case 'd':
      dontask=1;
      break;
    default:
      fprintf(stderr,"Bad argument.\n");
      // intentionally falls through
    case '?':
      usage();
    }
  }

  /* these need to be before the chroot and before the user swap
     so that it can get at the configuration file. This is 
     probably a security bug. */
  report=setup_user(distribution,sendmail);

  if(execute_filename){
    if((infile=fopen(execute_filename,"r"))==NULL){
      fprintf(stderr,"Can't seem to open file to read from.\n");
      perror(argv[0]);
      exit(BADOPS_EXIT);
    }
  }else{
    //open up /dev/random
    if((randfd=open(RAND_DEVICE,O_RDONLY))==-1){
      fprintf(stderr,"Cannot open %s.\n",execute_filename);
      exit(NORAND_EXIT);
    }

    if((infile=fdopen(randfd,"r"))==NULL){
      fprintf(stderr,"Can't open /dev/random for reading\n");
      exit(NORAND_EXIT);
    }
  }

  if((nullfd=open("/dev/null",O_WRONLY))==-1){
    perror("fuzz");
    fprintf(stderr,"Can't open /dev/null.\n");
    exit(CHILDFD_EXIT);
  }

  // end of stuff that must before chroot

  if(chr){
    char curpath[MAXPATH];
    if(getcwd(curpath,MAXPATH)==NULL){
      fprintf(stderr,"Can't get current path name to chroot to.\n");
      perror(argv[0]);
      usage();
    }
    if(chroot(curpath)==-1){
      fprintf(stderr,"Can't chroot.\n");
      perror(argv[0]);
      exit(CANT_CHROOT_EXIT);
    }
  }

  if(userinfo && setreuid(userinfo->pw_uid,userinfo->pw_uid)==-1){
    fprintf(stderr,"Can't change to user: %s\n",optarg);
    perror(argv[0]);
    exit(BADOPS_EXIT);
  }

  //make sure this isn't being run as root.
  if(getuid()==0){
    fprintf(stderr,"*** Don't run this program as root! ***\n");
    usage();
  }

  //check that the program exists
  if(optind==argc) //They didn't tell me what program to run
    usage();
  if(!(argv[optind][0]=='/' || argv[optind][0]=='.')){
    //if we don't know the full path already.
    char *modpath,*tok;
    if(getenv("PATH")==NULL){
      fprintf(stderr,"Warning: no path set using /bin:/usr/bin\n");
      path="/bin:/usr/bin";
    }else
      path=getenv("PATH");
    modpath=strdup(path);
    for(tok=strtok(modpath,":");tok!=NULL;tok=strtok(NULL,":")){
      struct stat statbuf;

      *progname=0;
      strncpy(progname,tok,MAXPATH);
      strncat(progname,"/",MAXPATH);
      strncat(progname,argv[optind],MAXPATH);
      if(stat(progname,&statbuf)==0){
	binfound=1;
	break;
      }
    }
  }else{
    // check that the binary is there
    struct stat statbuf;
    strcpy(progname,argv[optind]);
    if(stat(progname,&statbuf)==0)
      binfound=1;
  }
  if(!binfound){
    fprintf(stderr,"Program not found.\n");
    usage();
  }

  // setup the signals
  act.sa_handler=handle_sigalrm;
  if(sigemptyset(&act.sa_mask)==-1){
    fprintf(stderr,"can't clear signal mask.\n");
    abort();
  }
  act.sa_flags=0;
  sigaction(SIGALRM,&act,NULL);

  act.sa_handler=handle_sigchld;
  if(sigaddset(&act.sa_mask,SIGALRM)==-1){
    fprintf(stderr,"Can't add SIGALRM to signal mask.\n");
    abort();
  }
  sigaction(SIGCHLD,&act,NULL);

  // clearerr(infile);

  // run the program on the data sets
  for(;runs;runs--){
    int progpipe[2],status;
    char sendnewline=0;
    unsigned long curchar=0,linelen=0;

    // finish setting up files
    if(!execute_filename){
      snprintf(outfilename,MAXPATH,"/tmp%s.%lu",strrchr(progname,'/'),runs);
      if((outfile=fopen(outfilename,"w"))==NULL){
	fprintf(stderr,"Can't fopen outfile.\n");
	abort();
      }
      if((argfilename=strdup(outfilename))==NULL){
	fprintf(stderr,"Failed to strdup outfilename.\n");
	exit(ARGFILE_EXIT);
      }
      
      if((argfilename=realloc(argfilename,strlen(argfilename)+5))==0){
	fprintf(stderr,"Failed to realloc outfilename.\n");
	exit(ARGFILE_EXIT);
      }
      strcat(argfilename,".arg");
    }

    printf("Run: %u         %c",runs,print_bytes?'\n':'\r');
    if(pipe(progpipe)==-1){
      fprintf(stderr,"Can't pipe.\n");
      abort();
    }

    rundone=omit_data?1:0; // this implements the omit data feature

    if((newpid=fork())==-1){
      fprintf(stderr,"Can't fork.\n");
      abort();
    } else if(newpid==0){ //childproc
      do_child(progpipe,argv+optind,execute_filename,max_arglen,
	       printable_only,nullfd,randfd);
    }

    /**** handle main process */
    //start timer
    alarm(timeout);

    //feed chars down the pipe 
#ifdef HAVE_SENDFILE
    if(!print_bytes && !printable_only && !execute_filename && !linemod &&
       !rundone){
      off_t offset=0;
      if(sendfile(progpipe[1],randfd,&offset,len)==-1){
	fprintf(stderr,"sendfile failed\n");
	perror("fuzz");
	exit(NORAND_EXIT);
      }
    }else{
#endif
      for(curchar=0,linelen=0; !rundone && curchar<len; curchar++){
	char byte;
	
	if(print_bytes)
	  printf("\t%ld\r",curchar);
	if(!sendnewline){
	  do{
	    byte=fgetc(infile);
	    if(feof(infile)){
	      if(execute_filename){
		break; //we're done -- no problem
	      }else{
		fprintf(stderr,"Ran out of random data.\n");
		exit(NORAND_EXIT);
	      }
	    }
	  }while(printable_only && !isprint(byte));
	} else {
	  sendnewline=0;
	  byte='\n';
	}

	// make the race condition a bit harder to hit.
	if(rundone){
	  // fputs("lost race\n",stderr);
	  break;
	}
	if(write(progpipe[1],&byte,1)==-1){
	  if(errno==EINTR)
	    break;
	  else{
	    fprintf(stderr,"Write failed on pipe.\n");
	    perror(argv[0]);
	  }
	}
	if(!execute_filename)
	  fputc(byte,outfile);
	linelen++;
	if(byte=='\n')
	linelen=0;
	if((maxline!=0 && linelen==maxline) || 
	   (linemod!=0 && random()%linemod==0))
	  sendnewline=1;
      }
#ifdef HAVE_SENDFILE
    }
#endif

    //fputs("done with run\n",stderr);
    //clean up
    alarm(0); //cancle the alarm
    if(close(progpipe[0])==-1 ||close(progpipe[1])==-1){
      fprintf(stderr,"Warning: Can't close pipe ends.\n");
      perror(argv[0]);
    }
    kill(newpid,2);
    //    wait(&status);
    if(!execute_filename)
      fclose(outfile);
    if(unlink(outfilename)==-1){
      fprintf(stderr,"Can't unlink data file.\n");
      perror(argv[0]);
    }
    if(max_args && unlink(argfilename)==-1){
      fprintf(stderr,"Can't unlink arguments file.\n");
      perror(argv[0]);
    }
    fflush(stdout); 
/*     if(runs%10==0) */
/*       sleep(1); */
  }
  printf("Testing %s done -- No faults found.\n",progname);

  send_report(RESULT_PASSED, report, sendmail, distribution);
  exit(0);
}

/* this is for debugging use only */
void print_arglist(FILE* errs,char **argv,char expand_args, char hex){
  unsigned int i;
  fprintf(errs,"argv=0x%x\n",argv);
  for(i=0;argv[i]!=NULL;i++){
    fprintf(errs,"\targv[%u]=0x%x\n",i,argv[i]);
    if(expand_args){
      if(hex){
	unsigned int j;
	fprintf(errs,"argv[%u]: ",i);
	for(j=0;argv[i][j]!=0;j++){
	  fprintf(errs,"%0.2x ",argv[i][j]);
	}
	fputc('\n',errs);
      }else{
	fprintf(errs,"argv[%u]: %s\n",i,argv[i]);
      }// hex
    }// expand_args
  }// foreach arg
} // print_arglist


void handle_sigalrm(int dummy){
  int status;

  printf("\nProgram killed by signal.\n");
  signal(SIGCHLD,SIG_DFL);
  kill(newpid,9);
  wait(&status);
  printf("\n");
  exit(0);
}

void handle_sigchld(int dummy){
  int status,termsig;
  //  fputs("Program quit.\n",stderr);
  rundone=1;
  alarm(0);
  wait(&status);
  if(WIFSIGNALED(status) && ((termsig=WTERMSIG(status))==SIGILL || 
			     termsig==SIGSEGV || termsig==SIGBUS || 
			     termsig==SIGKILL)){
    printf("Program: %s\n\tProblem: CRASH\tSignal: %d\tRun: %lu\tDatafile: %s\n",
	   progname,termsig,runs,outfilename);
    send_report(RESULT_FAILED, report, sendmail, distribution);
    printf("\n");
    exit(0);
  }
  //  fputs("handler done\n",stderr);
}

void do_child(int *progpipe, char **argv,char *execute_filename,
	      unsigned long max_arglen,char printable_only,int nullfd,
	      int randfd){
  int cpy_stderr;
  char buf[80];
  unsigned int buflen;
  char **arguments=argv;
  
  //  fputs("child started\n",stderr);
  if(chdir("/tmp")==-1){
    perror("fuzz");
    fprintf(stderr,"Can't change working dir to /tmp.\n");
    exit(CHILDFD_EXIT);
  }
  // redo stdin
  if(dup2(progpipe[0],0)==-1){
    perror("fuzz");
    fprintf(stderr,"Can't dup2 stdin to pipe.\n");
    exit(CHILDFD_EXIT);
  }
  close(progpipe[0]);
  close(progpipe[1]);
  
  //redo stdout
  if(dup2(nullfd,1)==-1){
    perror("fuzz");
    fprintf(stderr,"Can't dup2 stdout to /dev/null.\n");
    exit(CHILDFD_EXIT);
  }
  
  // redo stderr
  //Make a copy of stderr so that I can report errors after I 
  // get rid or real stderr 
  if((cpy_stderr=dup(2))==-1){
    perror("fuzz");
    fprintf(stderr,"Can't dup stderr.\n");
    exit(CHILDFD_EXIT);
  }

  if(fcntl(cpy_stderr,F_SETFD)==-1){
    perror("fuzz");
    fprintf(stderr,"can't close on exec cpy_stderr\n");
    exit(CHILDFD_EXIT);
  }

  if(dup2(nullfd,2)==-1){
    perror("fuzz");
    write(cpy_stderr,"Can't dup2 stderr to /dev/null.\n",
	  strlen("Can't dup2 stderr to /dev/null.\n"));
    exit(CHILDFD_EXIT);
  }
  close(nullfd);
      
  // buflen=snprintf(buf,80,"checking args - max_args=%u\n",max_args);
  // write(cpy_stderr,buf,buflen);
  if(max_args){
    unsigned int numoldargs=0;
    unsigned int numargs=random()%max_args;
    unsigned int totalargs;
    size_t dummy;
    char **curarg;
    int argfilefd;
    FILE *errs;
    // write(cpy_stderr,"1\n",2);

    if((errs=fdopen(cpy_stderr,"w"))==NULL){
      write(cpy_stderr,"fdopen fails\n",strlen("fdopen fails\n"));
      exit(CHILDFD_EXIT);
    }
    
    // write(cpy_stderr,"2\n",2);
    if(execute_filename){
      unsigned int i;

      if(execute_filename=realloc(execute_filename,
				  strlen(execute_filename)+5)){
	fprintf(errs,"Can't realloc execute_filename.\n");
	exit(NOMEM_EXIT);
      }

      strcat(execute_filename,".arg");

      // write(cpy_stderr,"3\n",2);
      if((dummy=read(randfd,&totalargs,sizeof(totalargs)))==-1 || 
	 dummy!=sizeof(totalargs)){
	fprintf(errs,"Failed to read number of arguments.\n");
	exit(ARGFILE_EXIT);
      }

      if((arguments=curarg=argv=malloc((1+totalargs)*sizeof(char*)))==NULL){
	fprintf(errs,"Failed to malloc space for args.\n");
	exit(NOMEM_EXIT);
      }
      argv[totalargs]=NULL;

      for(i=0;i<totalargs;i++,curarg++){
	unsigned int paramlen;
	if((dummy=read(randfd,&paramlen,sizeof(paramlen)))==-1 || 
	   dummy!=sizeof(paramlen)){
	  fprintf(errs,"Failed to read number of arguments.\n");
	  exit(ARGFILE_EXIT);
	}
	if(((*curarg)=malloc(paramlen+1))==NULL){
	  fprintf(errs,"Failed to malloc space for arg.\n");
	  exit(NOMEM_EXIT);
	}
	(*curarg)[paramlen]=0;

	if((dummy=read(randfd,*curarg,paramlen))==-1 || dummy!=paramlen){
	  fprintf(errs,"Failed to read parameter.\n");
	  exit(ARGFILE_EXIT);
	}
      }
      close(randfd);
    }else{
      if((argfilefd=open(argfilename,O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|
			 S_IWUSR|S_IRGRP|S_IROTH))==-1){
	fprintf(errs,"Can't open argument file.\n");
	exit(ARGFILE_EXIT);
      }

      // count the number of arguments already in argv
      for(curarg=argv+1;(*curarg)!=NULL;curarg++){
	numoldargs++;

	// ***
	fprintf(errs,"counting - %s\n",*curarg);
	// fflush(errs);
      }

      // sleep(20);
      totalargs=numoldargs+numargs;

      // ***
      // fprintf(errs,"numargs = %u\nnumoldargs = %u\ntotargs = %u\n",numargs,
      // numoldargs,totalargs);

      if((arguments=curarg=malloc((totalargs+1)*sizeof(char*)))==NULL){
	fprintf(errs,"Not enough memory to allocate random args.\n");
	exit(NOMEM_EXIT);
      }

      // print_arglist(errs,arguments,0,0); // ***
      for(dummy=0;dummy<=totalargs;dummy++)
	arguments[dummy]=NULL;
      // print_arglist(errs,arguments,0,0); // ***
      

      if((dummy=write(argfilefd,&totalargs,sizeof(totalargs)))==-1 || 
	 dummy!=sizeof(totalargs)){
	fprintf(errs,"Can't write number of args.\n");
	exit(ARGFILE_EXIT);
      }

      // generate random arguments

      //copy the previous args.
      // fprintf(errs,"copying args\n"); // ***

      for(argv++;(*argv)!=NULL;curarg++,argv++){ 
	size_t arglen=strlen(*argv);

	
	// fprintf(errs,"copying arg\n"); // ***

	(*curarg)=(*argv);
	if((dummy=write(argfilefd,&arglen,sizeof(arglen)))==-1 || 
	   dummy!=sizeof(arglen)){
	  fprintf(errs,"Can't write old arg length.\n");
	  exit(ARGFILE_EXIT);
	}
	if((dummy=write(argfilefd,*curarg,arglen))==-1 || dummy!=arglen){
	  fprintf(errs,"Can't write old arg.\n");
	  exit(ARGFILE_EXIT);
	}

	// print_arglist(errs,arguments,0,0); // ***
      }

      // fprintf(errs,"generating new args\n"); // ***


      for(;numargs;numargs--,curarg++){
	unsigned long curarglen=random()%max_arglen;
	unsigned int i;

	// ***
	// print_arglist(errs,arguments,0,0); // ***
	// fprintf(errs,"-%x\n",curarg);
	// fprintf(errs,"--%x\n",curarg[0]);
	curarg[0]=malloc(curarglen+1);
	// fprintf(errs,"---%x\n",curarg[0]);

	if(curarg[0]==NULL){
	  fprintf(errs,"Not enough memory to allocate parameters.\n");
	  exit(NOMEM_EXIT);
	}
	// fputc('x',errs);
	// print_arglist(errs,arguments,0,0); // ***
	// fputc('x',errs);

	(*curarg)[curarglen]=0;

	if(read(randfd,(*curarg),curarglen)!=curarglen){
	  fprintf(errs,"Failed read on /dev/random. - %s\n",strerror(errno));
	  exit(NORAND_EXIT);
	}

	if(printable_only){
	  FILE *randfile=fopen(RAND_DEVICE,"r");
	  for(i=0;i<curarglen;i++){
	    while(!isprint((*curarg)[i])){
	      (*curarg)[i]=fgetc(randfile);
	    }
	  }
	  fclose(randfile);
	}

	// print_arglist(errs,arguments,0,0); // ***

	if((dummy=write(argfilefd,&curarglen,sizeof(curarglen)))==-1 || 
	   dummy!=sizeof(curarglen)){
	  fprintf(errs,"Can't write arg length.\n");
	  exit(ARGFILE_EXIT);
	}
	if((dummy=write(argfilefd,(*curarg),curarglen))==-1 || 
	   dummy!=curarglen){
	  fprintf(errs,"Can't write arg.\n");
	  exit(ARGFILE_EXIT);
	}
      }
    }
    if(fcntl(randfd,F_SETFD)==-1){
      fprintf(errs,"Cant set close on exec for randfd.\n");
      exit(CHILDFD_EXIT);
    }
    close(argfilefd);
    close(randfd);
  }

  //write(cpy_stderr,"execing test program\n",
  //      strlen("execing test program\n"));

  execv(progname,arguments);
  write(cpy_stderr,"Exec failed.\n",strlen("Exec failed.\n"));
  write(cpy_stderr,sys_errlist[errno],strlen(sys_errlist[errno]));
  exit(CANT_FORK_EXIT);
}

int setup_user(char *distribution, char *sendmail){
  char strbuf[MAXPATH];
  char *tmpbuf;
  int rcf;
  int fd;
  int tmp;
  struct stat statbuf;
  int report;
  
  snprintf(strbuf,MAXPATH-1,"%s/.fuzzrc",
	   (tmpbuf=getenv("HOME"))!=NULL?tmpbuf:".");
  if((rcf=open(strbuf,O_RDWR|O_CREAT,0644))==-1){
    perror("fuzz");
    exit(NORCFILE_EXIT);
  }
  switch(lseek(rcf,0,SEEK_END)){
  case 0: // no file existed
    // 1) prompt the user to see if we may report results
    report=0;
    printf("To make it possible to better coordinate the debugging of Linux, can fuzz \n"
	   "report results back to the master fuzz site using email? EVERY EFFORT WILL BE\n"
	   "MADE TO KEEP INFORMATION ANONYMOUS. Your email address will not be recorded or\n"
	   "disclosed to anyone including the fuzz developers. The email will contain only\nthe following information:\n"
	   "\t1. The operating system you are running.\n"
	   "\t2. The distribution that you are running.\n"
	   "\t3. Whether fuzz was able to automatically determine the distribution\n"
	   "\t   you were running.\n"	   
	   "\t4. The version of fuzz you are running.\n"
	   "\t5. The command line options passed to fuzz.\n"
	   "\t6. The name of the program being tested.\n"
	   "\t7. The version of the program being tested.\n"
	   "\t8. The dynamic libraries that it is linked against.\n"
	   "\t9. The results of the test.\n"
	   "If the program is part of an RPM or dpkg based distribuion:\n"
	   "\t10. The package that the program belogs to.\n"
	   "In the case where a program fails the fuzz test, then:\n"
	   "\t11. The data set which caused it to fail.\n");

    
    while(!report){
      printf("Allow fuzz to report findings? (Y/n) ");
      switch(getchar()){
      case '\n':
      case 'y':
      case 'Y':
	report=1;
	break;
      case 'n':
      case 'N':
	report=2;
	break;
      default:
	printf("\007\nDidn't understand that. ");
      }
    }
    // 2) figure out the distribution or ask
    /* Distribution:
       R read error on reading /etc/redhat-release
       S read error on reading /etc/debian_version
       K known redhat release
       D known debian release
       N unknown RPM based release
       O unknown dbkg based release
       U unknown distribution
       C known rpm based release changed by user
       B known dpkg based release changed by user
    */
    if(report!=1){
      report=0;
      if(write(rcf,"report=0\n",9)==-1){
	fprintf(stderr,"Can't write to .fuzzrc\n");
	perror("fuzz");
	exit(NORCFILE_EXIT);
      }
      if(write(rcf,"sendmail=0\n",9)==-1){
       fprintf(stderr,"Can't write to .fuzzrc\n");
       perror("fuzz");
       exit(NORCFILE_EXIT);
      }
    }else{
      if(write(rcf,"report=1\n",9)==-1){
	fprintf(stderr,"Can't write to .fuzzrc\n");
	perror("fuzz");
	exit(NORCFILE_EXIT);	
      }
      if(stat("/bin/rpm",&statbuf)!=-1){
	// rpm based release
	if((fd=open("/etc/redhat-release",O_RDONLY))!=-1){
	  if((tmp=read(fd,distribution+1,MAXPATH-1))==-1){
	    strcpy(distribution,"Runknown RPM based release");
	  }else{
	    char *cur;
	    distribution[0]='K';
	    chomp(distribution);
	  }
	  close(fd);
	}else{
	  strcpy(distribution,"Nunknown RPM based release");
	}
      }else if(stat("/bin/dpkg",&statbuf)==-1){
        if((fd=open("/etc/debian_version",O_RDONLY))!=-1){
          if((tmp=read(fd,distribution+1,MAXPATH-1))==-1){
            strcpy(distribution,"Sunknown dpkg based release");
          }else{
            char *cur;
            distribution[0]='D';
            chomp(distribution);
          }
          close(fd);
        }else{
          strcpy(distribution,"Ounknown dpkg based release");
        }
      }else{
	distribution[0]='U';
	distribution[1]='?';
      }
      snprintf(strbuf,MAXPATH-1,"\n\nDistribution? [%s] ",distribution+1);
      tmpbuf=readline(strbuf);
      if(tmpbuf!=NULL){
	if(tmpbuf[0]!=0){
	  if(distribution[0]='K' && strncmp(distribution+1,tmpbuf,MAXPATH))
            distribution[0]='C';
	  if(distribution[0]='D' && strncmp(distribution+1,tmpbuf,MAXPATH))
            distribution[0]='B';
	  strncpy(distribution+1,tmpbuf,MAXPATH-2);
	}
	free(tmpbuf);
      }
      tmp=snprintf(strbuf,MAXPATH-1,"dist_certainty=%c\ndist=%s\n",
		   distribution[0],distribution+1);
      if(write(rcf,strbuf,tmp)==-1){
	fprintf(stderr,"Can't write to .fuzzrc\n");
	perror("fuzz");
	exit(NORCFILE_EXIT);
      }
      // 3) Try to figure out what sendmail they can use
      if(stat(sendmail="/usr/lib/sendmail",&statbuf)==-1 &&
	 stat(sendmail="/usr/sbin/sendmail",&statbuf)==-1){
	do{
	  sendmail=readline("Where is sendmail? ");
	}while(stat(sendmail,&statbuf) && printf("Didn't understand that. "));
      }
      tmp=snprintf(strbuf,MAXPATH-1,"sendmail=%s\n",sendmail);
      if(write(rcf,strbuf,tmp)==-1){
	fprintf(stderr,"Can't write to .fuzzrc\n");
	perror("fuzz");
	exit(NORCFILE_EXIT);
      }
    }
    close(rcf);
    break;
  case -1: // bad things
    perror("fuzz");
    exit(NORCFILE_EXIT);
    break;
  default:
    // read the stuff from the file
    // hokey processing scheme
    lseek(rcf,0,SEEK_SET);
    if(read(rcf,strbuf,MAXPATH)==-1){
      fprintf(stderr,"Read failed on .fuzzrc\n");
      perror("fuzz");
      exit(NORCFILE_EXIT);
    }
    for(tmpbuf=strtok(strbuf,"\n");tmpbuf!=NULL;tmpbuf=strtok(NULL,"\n")){
      if(!strncmp(tmpbuf,"report=",7)){
	if(sscanf(tmpbuf,"report=%d",&report)!=1){
	  fprintf(stderr,"Can't read report from .fuzzrc\n");
	  exit(NORCFILE_EXIT);
	}else if ( report == 0 ) 
	  break;
      }else if(!strncmp(tmpbuf,"dist_certainty=",15)){
	if(sscanf(tmpbuf,"dist_certainty=%c",distribution)!=1){
	  fprintf(stderr,"Can't read dist_certainty from .fuzzrc\n");
	  exit(NORCFILE_EXIT);
	}
      }else if(!strncmp(tmpbuf,"dist=",5)){
	strcpy(distribution+1,tmpbuf+5);
      }else if(!strncmp(tmpbuf,"sendmail=",9)){
	if(sscanf(tmpbuf,"sendmail=%s",sendmail)!=1){
	  fprintf(stderr,"Can't read sendmail from .fuzzrc\n");
	  exit(NORCFILE_EXIT);
	}
      }else{
	fprintf(stderr,"Corrupt .fuzzrc\n");
	exit(NORCFILE_EXIT);
      }
    }
    if(report){
      if((*distribution!='R' && *distribution!='K' && *distribution!='N' && 
	  *distribution!='D' && *distribution!='U' && *distribution!='C') || 
	 strlen(distribution+1)==0 || strlen(sendmail)==0){
	fprintf(stderr,"Not all required variables are present in .fuzzrc\n"); 
	exit(NORCFILE_EXIT);
      }
    }
  } // end of case where we get info from rcfile or from user
  return report;
}

void send_report(char result, char report, char *sendmail, char *distribution){
  if(report){
    FILE *mail,*version;
    int i;
    char strbuf[MAXPATH],*tmp;
    time_t tm;

    if(dontask)
      tmp=strdup("NULL");
    else
      tmp=readline("Version of program tested? ");

    fputs("Sending report",stdout);
    fflush(stdout);
    snprintf(strbuf,MAXPATH-1,"%s %s %s",sendmail,SENDMAILARGS,DESTADDR);
    
    if((mail=popen(strbuf,"w"))==NULL){
      fprintf(stderr,"can't send report\n");
      exit(NOREPORT_EXIT);
    }

    time(&tm);
    fprintf(mail,"From: fuzz program\nTo: %s\n"
	    "Subject: results\nDate: %s\n",DESTADDR,ctime(&tm));

    // item 1 the OS
    fputs(" 1",stdout);
    fflush(stdout);
    if((version=popen("uname -a","r"))==NULL){
      fprintf(stderr,"Can't figure out system type with uname -a.\n");
      perror("fuzz");
      exit(NOREPORT_EXIT);
    }
    fgets(strbuf,MAXPATH-1,version);
    //consume remaining data
    while(!feof(version)){
      fgets(strbuf,MAXPATH-1,version);
    }
    pclose(version);
    fprintf(mail,"os=%s",strbuf);

    // item 2-4 the distribution and version of fuzz
    fputs(" 2 3 4",stdout);
    fflush(stdout);
    fprintf(mail,"distribution=%s\nfuzz_version=%s\n",distribution,VERSION);

    // item 5 the command line options passed to fuzz
    fputs(" 5",stdout);
    fflush(stdout);
    fprintf(mail,"num_args=%d\n",g_argc);
    for(i=0;i<g_argc;i++){
      fprintf(mail,"arg[%d]=\"%s\"\n",i,g_argv[i]);
    }
    
    // item 6-7 the program that is being tested
    fputs(" 6 7",stdout);
    fflush(stdout);
    fprintf(mail,"program_name=%s\nprogram_version=%s\n",progname,tmp);
    free(tmp);

    // item 8 the libraries that the program is linked against
    fputs(" 8",stdout);
    fflush(stdout);
    snprintf(strbuf,MAXPATH-1,"ldd %s",progname);
    if((version=popen(strbuf,"r"))==NULL){
      fprintf(stderr,"Can't run ldd.\n");
      perror("fuzz");
      exit(NOREPORT_EXIT);
    }
    for(i=0;!feof(version);i++){
      char *cur;
      struct stat statbuf;

      fgets(strbuf,MAXPATH-1,version);
      if(!strstr(strbuf,"not a dynamic executable"))
	break;
      if(feof(version))
	break;
      if((cur=strstr(strbuf," => "))==NULL){
	fprintf(stderr,"Error processing ldd output -- can't find =>\n");
	exit(NOREPORT_EXIT);
      }
      cur+=4;
      if(strtok(cur," ")==NULL){
	fprintf(stderr,"Error processing ldd output -- can't find space\n");
	exit(NOREPORT_EXIT);
      }

      if(lstat(cur,&statbuf)==-1){
	fprintf(stderr,"Can't stat library %s.\n",cur);
	exit(NOREPORT_EXIT);
      }
      if(S_ISLNK(statbuf.st_mode)){
	char tmpbuf[MAXPATH];
	memset(tmpbuf,0,MAXPATH);
	if(readlink(cur,tmpbuf,MAXPATH-1)==-1){
	  fprintf(stderr,"Can't readlink library %s\n",cur);
	  perror("fuzz");
	  exit(NOREPORT_EXIT);
	}
	fprintf(mail,"library[%d]=%s\n",i,tmpbuf);
      }else{
	fprintf(mail,"library[%d]=%s\n",i,cur);
      }
    }
    pclose(version);

    // item 9 the results of the test
    fputs(" 9",stdout);
    fflush(stdout);
    fprintf(mail,"results=%s\n", result==RESULT_PASSED?"pass":"fail");

    // item 10 the packaging info
    fputs(" 10",stdout);
    fflush(stdout);
    strbuf[0]=0;
    fprintf(mail,"version=");
    if(distribution[0]=='K' || distribution[0]=='N' || distribution[0]=='C' || 
       distribution[0]=='R'){
      snprintf(strbuf,MAXPATH-1,"rpm -qf %s",progname);
      if((version=popen(strbuf,"r"))==NULL){
        fprintf(stderr,"Can't execute %s\n",strbuf);
	fprintf(mail,"UNK1\n.\n");
	exit(NOREPORT_EXIT);
      }
      if(fgets(strbuf,MAXPATH-1,version)==NULL){
	fprintf(stderr,"strange output from rpm -qf\n");
	fprintf(mail,"UNK2\n.\n");
	exit(NOREPORT_EXIT);
      }
      fputs(strbuf,mail);
      //consume remaining data
      while(!feof(version)){
	fgets(strbuf,MAXPATH-1,version);
      }
      pclose(version);
    } else if(distribution[0]=='S' || distribution[0]=='O' || 
              distribution[0]=='B' || distribution[0]=='D'){
      snprintf(strbuf,MAXPATH-1,"dpkg -S %s",progname);
      if((version=popen(strbuf,"r"))==NULL){
        fprintf(stderr,"Can't execute %s\n",strbuf);
	fprintf(mail,"UNK\n.\n");
	exit(NOREPORT_EXIT);
      }
      if(fgets(strbuf,MAXPATH-1,version)==NULL){
        fprintf(stderr,"Can't read dpkg -S output.\n");
        fprintf(mail,"UNK2\n.\n");
        exit(NOREPORT_EXIT);
      }
      //consume remaining data
      while(!feof(version)){
	fgets(strbuf,MAXPATH-1,version);
      }
      pclose(version);
      if(strtok(strbuf,":")==NULL){
        fprintf(stderr,"Strange output from dpkg -S.\n");
        fprintf(mail,"UNK3\n.\n");
        exit(NOREPORT_EXIT);
      }
      tmp=strdup(strbuf);
      fprintf(mail,"%s-",tmp);
      snprintf(strbuf,MAXPATH-1,"dpkg -s %s",tmp);
      free(tmp);
      if((version=popen(strbuf,"r"))==NULL){
        fprintf(stderr,"Can't execute %s\n",strbuf);
	fprintf(mail,"UNK4\n.\n");
	exit(NOREPORT_EXIT);
      }
      while(!feof(version)){
	fgets(strbuf,MAXPATH-1,version);
	if(feof(version))
	  break;
        if(!strncmp(strbuf,"Version: ",9))
          fputs(strbuf+9,mail);
      }
      pclose(version);      
    }

    // item 11 the data set which caused the crash
    fputs(" 11",stdout);
    fflush(stdout);
    if(result==RESULT_PASSED){
      fputs(".\n",mail); // end the mail
      fputs(" done\n",stdout);
      fflush(stdout);
    }else{
      // send all the pertinent data
      int fd,len,i;
      
      fprintf(mail,"data\n");
      if((fd=open(outfilename,O_RDONLY))==-1){
	// this should never happen
	fprintf(stderr,"can't reopen outfile.\n");
	perror("fuzz");
	pclose(mail);
	exit(NOREPORT_EXIT);
      }
      while((len=read(fd,strbuf,MAXPATH))!=-1 && len!=0){
	for(i=0;i<len;i++){
	  fprintf(mail,"%2.2x ",strbuf[i]&0xff);
	  if(i%25==0)
	    fputc('\n',mail);
	}
      }
      close(fd);
      fputs("\nend\n",mail);

      if(max_args){
	fprintf(mail,"args\n");
	
	if((fd=open(argfilename,O_RDONLY))==-1){
	  // this should never happen
	  fprintf(stderr,"can't reopen argfilename.\n");
	  perror("fuzz");
	  pclose(mail);
	  exit(NOREPORT_EXIT);
	}
	while((len=read(fd,strbuf,MAXPATH))!=-1 && len!=0){
	  for(i=0;i<len;i++){
	    fprintf(mail,"%x ",strbuf[i]);
	    if(i%25==0)
	      fputc('\n',mail);
	  }
	}
	close(fd);
	fputs("\nend\n",mail);
      }
      pclose(mail);
      fputs(" done\n",stdout);
      fflush(stdout);
    }
  }
}
